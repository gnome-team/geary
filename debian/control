Source: geary
Section: mail
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Daniel Kahn Gillmor <dkg@fifthhorseman.net>,
           Jeremy Bícha <jbicha@ubuntu.com>,
           Laurent Bigonville <bigon@debian.org>
Build-Depends: debhelper-compat (= 13),
               appstream-util <!nocheck>,
               at-spi2-core <!nocheck>,
               ca-certificates <!nocheck>,
               dbus <!nocheck>,
               desktop-file-utils,
               gnutls-bin <!nocheck>,
               iso-codes,
               itstool,
               libappstream-glib-dev (>= 0.7.10),
               libenchant-2-dev (>= 2.1),
               libfolks-dev (>= 0.11),
               libgcr-3-dev (>= 3.10.1),
               libgee-0.8-dev (>= 0.8.5),
               libgirepository1.0-dev (>= 1.32.0),
               libglib2.0-dev (>= 2.68),
               libgmime-3.0-dev (>= 3.2.4),
               libgoa-1.0-dev (>= 3.45),
               libgsound-dev,
               libgspell-1-dev,
               libgtk-3-dev (>= 3.24.23),
               libhandy-1-dev (>= 1.6.0),
               libicu-dev (>= 60),
               libjson-glib-dev (>= 1.0),
               libmessaging-menu-dev (>= 12.10),
               libpeas-dev (>= 1.24.0),
               librsvg2-common <!nocheck>,
               libsecret-1-dev (>= 0.11),
               libsqlite3-dev (>= 3.24),
               libstemmer-dev,
               libunwind-dev (>= 1.1) [amd64 arm64 armel armhf i386 mips64el ppc64el riscv64 s390x hppa loong64 powerpc ppc64 sh4],
               libwebkit2gtk-4.1-dev,
               libxml2-dev (>= 2.7.8),
               libytnef0-dev (>= 1.9.3),
               localehelper <!nocheck>,
               locales <!nocheck>,
               meson (>= 0.59),
               valac (>= 0.48.11),
               xauth <!nocheck>,
               xvfb <!nocheck>
Standards-Version: 4.7.0
Homepage: https://wiki.gnome.org/Apps/Geary
Vcs-Git: https://salsa.debian.org/gnome-team/geary.git
Vcs-Browser: https://salsa.debian.org/gnome-team/geary
Rules-Requires-Root: no

Package: geary
Architecture: any
Depends: gnome-keyring | kwalletmanager, ${misc:Depends}, ${shlibs:Depends}
Provides: imap-client, mail-reader
Description: lightweight email client designed for the GNOME desktop
 Geary is an email reader for GNOME designed to let you read your email
 quickly and effortlessly. Its interface is based on conversations, so you
 can easily read an entire discussion without having to click from message
 to message.
